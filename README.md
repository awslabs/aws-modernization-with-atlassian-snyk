# AWS Modernization Workshop: DevSecOps with Atlassian & Snyk

![Atlassian and Snyk](content/images/atlassian-snyk-header.png)

In this workshop, you will learn patterns for shift-left security leveraging [Atlassian Bitbucket](https://www.atlassian.com/software/bitbucket), [Bitbucket Pipelines](https://bitbucket.org/product/features/pipelines), 
and [Snyk](https://snyk.io). These techniques will enable you to implement scanning of your container-based workloads running on 
[Amazon Elastic Kubernetes Service (Amazon EKS)](https://aws.amazon.com/eks/) and [Amazon Elastic Container Registry (ECR)](https://aws.amazon.com/ecr/) 
and how to use these patterns to release features and functionality at a faster pace that includes security at each step.

Visit https://snyk-atlassian.awsworkshop.io to begin the workshop!

